# Title - app for something

## Table of contents

- [General info](#general-info)
- [Live preview](#live-preview)
- [Technologies](#technologies)
- [Features](#features)
- [Screenshots](#screenshots)
- [Setup](#setup)
- [Project status](#project-status)
- [Room for Improvement](#room-for-improvement)

## General info

This project has no details yet

## Live preview

Here will be link to the live preview in the future

## Technologies

Project is created with:

- PHP: 8.0.16
- JavaScript
- CSS

## Features

- First feature
- Second feature

## Screenshots

Here will be screenshot of the app in the future

## Setup

To run this project, you have to have docker installed on yur computer

```
$ cd wdpai
$ docker-compose up -d
```

## Project status

Project is on a very early stage

## Room for Improvement

Here are nice to have ideas for features and also TODOs

Ideas:

- Idea 1
- Idea 2

TODO:

- Find an idea
- Make a design
