<?php

class Student
{
  private $surname;
  private $name;

  function __construct(string $surname, string $name)
  {
    $this->surname = $surname;
    $this->name = $name;
  }

  function setSurname(string $surname)
  {
    return $this->surname = $surname;
  }

  function setName(string $name)
  {
    return $this->name = $name;
  }

  function getName()
  {
    return $this->name;
  }

  function getSurname()
  {
    return $this->surname;
  }

  function __toString()
  {
    return "Student: " . $this->getName() . " " . $this->getSurname();
  }
}
