<?php

class AppController
{
  protected function asView(string $templateName = 'index', array $arguments = [])
  {
    $filepath = 'public/views/' . $templateName . '.html';
    $output = "Page not found.";

    if (file_exists($filepath)) {

      extract($arguments);

      ob_start();
      include $filepath;
      $output = ob_get_clean();
    }
    print $output;
  }
}
