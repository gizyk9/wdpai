<?php

require_once __DIR__ . '/AppController.php';

class ErrorPageController extends AppController
{
  public function error404()
  {
    $this->asView('404');
  }
}
