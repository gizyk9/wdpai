<?php

require_once __DIR__ . '/AppController.php';

class DashboardController extends AppController
{
  public function index(string $arguments)
  {
    $this->asView('dashboard', [
      "number" => $arguments,
    ]);
  }
}
