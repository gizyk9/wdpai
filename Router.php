<?php

require_once __DIR__ . '/controllers/DashboardController.php';
require_once __DIR__ . '/controllers/ErrorPageController.php';

class Router
{
  private static $controllerRoutes = [];

  public static function get(string $path, string $controllerClass)
  {
    if (isset(self::$controllerRoutes[$path])) {
      die("Controller already exists!");
    }

    self::$controllerRoutes[$path] = $controllerClass;
  }

  public static function run(string $urlPath)
  {
    $urlPathPartials = explode('/', $urlPath);
    $path = $urlPathPartials[0];

    if (!isset(self::$controllerRoutes[$path])) {
      http_response_code(404);
      $errorPage = new ErrorPageController();
      $errorPage->error404();
      die();
    }

    $controller = new self::$controllerRoutes[$path];
    $arguments = $urlPathPartials[1] ?? '';

    $controller->index($arguments);
  }
}
