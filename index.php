<?php

require __DIR__ . '/Router.php';
// require("./app/student.php");

// echo "Hello world!<br>";

// $student = new Student("Kowalski", "Jan");

// echo $student;

// $connStr = "pgsql:host=db;port=5432;dbname=postgres;user=postgres;password=example";
// $conn = new PDO($connStr);
// $sql = "select * from pg_stat_activity";
// foreach ($conn->query($sql) as $row) {
//   var_dump($row);
// }

Router::get('dashboard', DashboardController::class);

$path = trim($_SERVER['REQUEST_URI'], '/');
Router::run($path);
